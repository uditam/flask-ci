FROM tiangolo/uwsgi-nginx-flask:latest

# RUN apk --update add bash nano

ENV CONDUIT_SECRET something-really-secret
ENV FLASK_APP ./autoapp.py
ENV FLASK_DEBUG 1

RUN pip install --upgrade pip

COPY . /app
WORKDIR /app

RUN pip install -r requirements/dev.txt